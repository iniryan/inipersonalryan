import React, { useState, useEffect, useRef } from "react";
import "./Project.css";

import frame1 from "../../assets/mejapintar.png";
import frame2 from "../../assets/pepsi.png";
import frame3 from "../../assets/ip11.png";
import frame4 from "../../assets/chatapp.png";

import { projectss } from "../ScrolltriggerGsap";

function Project() {
  const [title] = useState({
    title: "Projects",
  });
  const [project] = useState([
    {
      id: 1,
      link: (
        <a href="/#blank-project" className="link">
          View Project ⇾
        </a>
      ),
      img: <img src={frame1} alt="project1" />,
      titles: "Mejapintar",
      desc:
        "This is my very first project when it comes to website development. This project created to helping student learn about basic lesson like math, sains, etc. ",
      used: (
        <ul className="used-items">
          <li className="used-item">PHP</li>
          <li className="used-item">Mysql</li>
        </ul>
      ),
    },
    {
      id: 2,
      link: (
        <a href="/#blank-project" className="link">
          View Project ⇾
        </a>
      ),
      img: <img src={frame2} alt="project2" />,
      titles: "Pepsi Design Inspiration Concept",
      desc:
        "This is one of my few projects about web design. This project is redesign from pepsi.com website. I hope you like my design inspiration concept.",
      used: (
        <ul className="used-items">
          <li className="used-item">Figma</li>
        </ul>
      ),
    },
    {
      id: 3,
      link: (
        <a href="/#blank-project" className="link">
          View Project ⇾
        </a>
      ),
      img: <img src={frame3} alt="project3" />,
      titles: "Iphone 11 Pro Design Inspiration Concept",
      desc:
        "This is another one of my few projects about web design. This design used to promote apple smartphone product, Iphone 11 Pro. I hope you like my design concept.",
      used: (
        <ul className="used-items">
          <li className="used-item">Figma</li>
        </ul>
      ),
    },
    {
      id: 4,
      link: (
        <a href="/#blank-project" className="link">
          View Project ⇾
        </a>
      ),
      img: <img src={frame4} alt="project4" />,
      titles: "Chat App Mobile Design Inspiration Concept",
      desc:
        "This too is another one of my few projects design about mobile app design. This project made by myself and this is'nt redesign. I hope you like my design inspiration concept.",
      used: (
        <ul className="used-items">
          <li className="used-item">Figma</li>
        </ul>
      ),
    },
  ]);

  let projectcs = useRef(null);
  useEffect(() => {
    projectss(projectcs);
  });
  
  return (
    <div className="container" id="project" ref={(el) => (projectcs = el)}>
      <div className="wrapper">
        <div className="common">
          <h3 className="title">
            {title.title}
            <span>.</span>
          </h3>
        </div>
        <div className="featured-projects">
          {project.map((info) => (
            <div key={info.id} className="">
              <div className="project">
                <figure className="project-pict">{info.img}</figure>

                <figcaption className="project-caption">
                  <div className="project-name">{info.titles}</div>
                  <p>{info.desc}</p>
                  {info.link}
                  <div className="used">{info.used}</div>
                </figcaption>
              </div>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
}

export default Project;
